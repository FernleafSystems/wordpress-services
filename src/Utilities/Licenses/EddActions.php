<?php

namespace FernleafSystems\Wordpress\Services\Utilities\Licenses;

class EddActions {

	/**
	 * @param string $sUrl
	 * @return string
	 */
	public static function CleanUrl( $sUrl ) {
		return sanitize_text_field( trailingslashit(
			preg_replace( '#^(https?:/{1,2})?(www\.)?#', '', mb_strtolower( trim( $sUrl ) ) )
		) );
	}
}