<?php

namespace FernleafSystems\Wordpress\Services\Utilities\Licenses;

use FernleafSystems\Utilities\Data\Adapter\StdClassAdapter;
use FernleafSystems\Wordpress\Services\Services;

/**
 * Class LookupKeyless
 * @package FernleafSystems\Wordpress\Services\Utilities\Licenses
 * @property string $lookup_url_stub
 * @property int    $timeout
 * @property int    $item_id
 * @property string $install_id
 * @property string $check_url
 * @property string $nonce
 * @property array  $meta
 */
class LookupKeyless {

	use StdClassAdapter;
	const DEFAULT_URL_STUB = 'https://www.shieldsecurity.io/wp-json/odp-eddkeyless/v1/lookup';

	/**
	 * @return EddLicenseVO
	 */
	public function lookup() {
		$this->check_url = Services::WpGeneral()->getHomeUrl( '', true );
		if ( empty( $this->lookup_url_stub ) ) {
			$this->lookup_url_stub = self::DEFAULT_URL_STUB;
		}

		$aRaw = $this->sendReq( false );
		if ( is_array( $aRaw ) && !empty( $aRaw[ 'keyless' ] ) && !empty( $aRaw[ 'keyless' ][ 'license' ] ) ) {
			$aLicenseInfo = $aRaw[ 'keyless' ][ 'license' ];
		}
		else {
			$aLicenseInfo = [];
		}

		$oLic = ( new EddLicenseVO() )->applyFromArray( $aLicenseInfo );
		$oLic->last_request_at = Services::Request()->ts();
		return $oLic;
	}

	/**
	 * @param string $sItemId
	 * @return EddLicenseVO
	 */
	public function lookupByItem( $sItemId ) {
		$this->item_id = $sItemId;
		return $this->lookup();
	}

	/**
	 * first attempts GET, then POST if the GET is successful but the data is not right
	 * @param bool $bAsPost
	 * @return array
	 */
	private function sendReq( $bAsPost = false ) {
		$oHttpReq = Services::HttpRequest();

		$sUrl = sprintf( '%s/%s/%s', rtrim( $this->lookup_url_stub, '/' ), $this->item_id, $this->install_id );
		$aParams = [
			'timeout' => empty( $this->timeout ) ? 60 : $this->timeout,
			'body'    => array_intersect_key(
				$this->getRawDataAsArray(),
				array_flip( [
					'check_url',
					'nonce',
					'meta',
				] )
			)
		];

		$aResponse = [];
		if ( $bAsPost ) {
			if ( $oHttpReq->post( $sUrl, $aParams ) ) {
				$aResponse = empty( $oHttpReq->lastResponse->body ) ? [] : @json_decode( $oHttpReq->lastResponse->body, true );
			}
		}
		elseif ( $oHttpReq->get( $sUrl, $aParams ) ) {
			$aResponse = empty( $oHttpReq->lastResponse->body ) ? [] : @json_decode( $oHttpReq->lastResponse->body, true );
			if ( empty( $aResponse ) ) {
				$aResponse = $this->sendReq( true );
			}
		}

		return $aResponse;
	}
}