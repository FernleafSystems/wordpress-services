<?php

namespace FernleafSystems\Wordpress\Services\Utilities\Integrations\WpHashes\Token;

use FernleafSystems\Wordpress\Services\Utilities\Licenses\EddActions;

/**
 * Class Solicit
 * @package FernleafSystems\Wordpress\Services\Utilities\Integrations\WpHashes\Token
 */
class Solicit extends Base {

	/**
	 * @param string $sUrl
	 * @param string $sInstallId
	 * @return array|null
	 */
	public function retrieve( $sUrl, $sInstallId ) {
		/** @var RequestVO $oReq */
		$oReq = $this->getRequestVO();
		$oReq->action = 'solicit';
		$oReq->install_id = $sInstallId;
		$oReq->url = untrailingslashit( EddActions::CleanUrl( $sUrl ) );
		return $this->query();
	}

	/**
	 * @return string
	 */
	protected function getApiUrl() {
		$aData = array_filter( array_merge(
			[
				'action'     => false,
				'install_id' => false,
				'url'        => false,
			],
			$this->getRequestVO()->getRawDataAsArray()
		) );
		return sprintf( '%s/%s', parent::getApiUrl(), implode( '/', $aData ) );
	}
}